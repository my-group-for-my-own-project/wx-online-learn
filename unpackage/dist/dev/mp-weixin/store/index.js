"use strict";
const common_vendor = require("../common/vendor.js");
const store_user = require("./user.js");
common_vendor.createStore({
  module: {
    user: store_user.user
  }
});
