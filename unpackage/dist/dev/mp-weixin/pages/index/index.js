"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  __name: "index",
  setup(__props) {
    const menulist = common_vendor.reactive(["今日推荐", "免费课程", "实战课程"]);
    let checkedNum = common_vendor.ref(0);
    const bannerList = common_vendor.reactive([
      "../../static/photo/bg/js1.png",
      "../../static/photo/bg/python-img.png",
      "../../static/photo/bg/js3.png"
    ]);
    const mainData = common_vendor.reactive({
      arr: [{
        "join_num": 16,
        "level": "零基础",
        "name": "晋级JavaScript高手，成为热门项目高手",
        "people": 25e4,
        "price": "999.00",
        "url": "../../static/photo/bg/js3.png"
      }, {
        "join_num": 6,
        "level": "初级",
        "name": "晋级Python高手，成为热门项目高手",
        "people": 5e5,
        "price": "1999.00",
        "url": "../../static/photo/bg/python-img.png"
      }, {
        "join_num": 88,
        "level": "高级",
        "name": "晋级JavaScript高手，成为热门项目高手",
        "people": 1e5,
        "price": "2999.00",
        "url": "../../static/photo/bg/js1.png"
      }, {
        "join_num": 77,
        "level": "零基础",
        "name": "晋级JavaScript高手，成为热门项目高手",
        "people": 7e5,
        "price": "888.00",
        "url": "../../static/photo/bg/js2.png"
      }, {
        "join_num": 21,
        "level": "零基础",
        "name": "晋级JavaScript高手，成为热门项目高手",
        "people": 8e5,
        "price": "777.00",
        "url": "../../static/photo/bg/js1.png"
      }, {
        "join_num": 65,
        "level": "零基础",
        "name": "晋级Python高手，成为热门项目高手",
        "people": 56e4,
        "price": "666.00",
        "url": "../../static/photo/bg/python-img.png"
      }, {
        "join_num": 87,
        "level": "中级",
        "name": "晋级JavaScript高手，成为热门项目高手",
        "people": 32e4,
        "price": "799.00",
        "url": "../../static/photo/bg/js2.png"
      }, {
        "join_num": 16,
        "level": "零基础",
        "name": "晋级JavaScript高手，成为热门项目高手",
        "people": 1e6,
        "price": "899.00",
        "url": "../../static/photo/bg/js3.png"
      }, {
        "join_num": 11,
        "level": "高级",
        "name": "晋级JavaScript高手，成为热门项目高手",
        "people": 2e5,
        "price": "2899.00",
        "url": "../../static/photo/bg/js1.png"
      }, {
        "join_num": 72,
        "level": "初级",
        "name": "晋级JavaScript高手，成为热门项目高手",
        "people": 1e5,
        "price": "5999.00",
        "url": "../../static/photo/bg/js2.png"
      }]
    });
    const changeMenu = (index) => {
      checkedNum.value = index;
    };
    const changeSwiper = (e) => {
      checkedNum.value = e.detail.current;
    };
    let _this = common_vendor.getCurrentInstance();
    let heightData = common_vendor.ref(null);
    const setSwiperHeight = () => {
      let query = common_vendor.index.createSelectorQuery().in(_this);
      let classStr = `.swiper-item-main-${checkedNum.value}`;
      query.selectAll(classStr).boundingClientRect((data) => {
        if (data[0]) {
          heightData.value = data[0].height;
        }
      }).exec();
    };
    common_vendor.onMounted(() => {
      setSwiperHeight();
    });
    common_vendor.watch(checkedNum, () => {
      setSwiperHeight();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(menulist, (item, index, i0) => {
          return {
            a: common_vendor.t(item),
            b: common_vendor.n(common_vendor.unref(checkedNum) === index ? "" : "hide"),
            c: item,
            d: common_vendor.n(common_vendor.unref(checkedNum) === index ? "checked" : ""),
            e: common_vendor.o(($event) => changeMenu(index), item)
          };
        }),
        b: common_vendor.f(bannerList, (item, index, i0) => {
          return {
            a: item,
            b: index
          };
        }),
        c: common_vendor.f(mainData.arr.slice(0, 3), (item, index, i0) => {
          return {
            a: item.url,
            b: common_vendor.t(item.name),
            c: common_vendor.t(item.level),
            d: common_vendor.t(Math.trunc(item.price)),
            e: common_vendor.t(item.people / 1e3),
            f: index
          };
        }),
        d: common_vendor.f(mainData.arr, (item, index, i0) => {
          return {
            a: item.url,
            b: common_vendor.t(item.level),
            c: common_vendor.t(item.join_num),
            d: common_vendor.t(Math.trunc(item.price * 0.8)),
            e: common_vendor.t(Math.trunc(item.price))
          };
        }),
        e: common_vendor.f(mainData.arr, (item, index, i0) => {
          return {
            a: item.url,
            b: common_vendor.t(item.name),
            c: common_vendor.t(item.level),
            d: common_vendor.t(item.people / 1e3),
            e: index
          };
        }),
        f: common_vendor.t(0),
        g: common_vendor.f(mainData.arr, (item, index, i0) => {
          return {
            a: item.url,
            b: common_vendor.t(item.name),
            c: common_vendor.t(item.level),
            d: common_vendor.t(Math.trunc(item.price)),
            e: common_vendor.t(item.people / 1e3),
            f: index
          };
        }),
        h: common_vendor.unref(checkedNum),
        i: common_vendor.o(changeSwiper),
        j: common_vendor.s(`height: ${common_vendor.unref(heightData)}px`)
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-1cf27b2a"], ["__file", "G:/Work_web/Uni-app/online-education/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
