// import Vue from 'vue'
import {createStore} from 'vuex'
import user from './user.js'

// Vue.use(Vuex)

const store = createStore({
	module:{
		user,
	},
})

export default  store